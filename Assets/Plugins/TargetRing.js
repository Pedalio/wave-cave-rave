﻿//#pragma strict

//VARIABLES:
var playerPosition : Vector3;
var inputAim : Vector3;
var inputMove : Vector3;
var ringDistance : float = 1.5f;
var throwSpeed : float = 800;

var player : GameObject;
var objectClosest : GameObject = null;
var objectCarried : GameObject = null;

//STATES:
enum TargetState{Off, Tight, TightLocked, Loose, Range};

	var currentState : TargetState;

function Start () {

	currentState = TargetState.Off;

}

//MAIN LOOP:
function Update () {
	
	//Get input from parent
	playerPosition = transform.parent.position;
	inputAim = GetComponentInParent.<Player>().inputAim;
	inputMove = GetComponentInParent.<Player>().inputMove;
	
	//Determine State
	//if(GetComponentInParent.<Player>().stateTop == State.Carry){
		TightLocked();
	//}else if(inputAim.magnitude > .02){
	//	Tight();
	//}else{
		//Off();
	//	Tight();
	//}
}

//TRIGGER:
function OnTriggerStay(other : Collider){

	//Check if the object has a properties script
	if(other.GetComponent.<Properties>() != null){
		//If it does & it's pickupable, then:
		if(other.GetComponent.<Properties>().pickupable == true){
			
			//Set the closest object if there is none
			if(objectClosest == null){
				objectClosest = other.gameObject;
			}
			
			//Calculate closest object
			var distanceOther = Vector3.Distance(transform.position, other.transform.position);
			var distanceClosest = Vector3.Distance(transform.position, objectClosest.transform.position);
			
			//Assign closest object
			if(distanceOther < distanceClosest){
				objectClosest = other.gameObject;
			}
		}
	}
}

function OnTriggerExit(other : Collider){
	//Reset closest object
	objectClosest = null;
}
	
//FUNCTIONS:

function Carry(){
	//If there's an object to pick up, then:
	if(objectClosest != null && objectCarried == null){
		//assign object
		objectCarried = objectClosest;
	}
	if(objectCarried != null){
		//Change the player state
		GetComponentInParent.<Player>().Carry();
		//Apply object constraints
		objectCarried.GetComponent.<Rigidbody>().useGravity = false;
		//objectCarried.GetComponent.<Rigidbody>().isKinematic = true; //WAS TURNED ON
		//objectCarried.GetComponent.<Collider>().enabled = false; //WAS TURNED ON
		
		//Move the object
		objectCarried.transform.position = Vector3.Lerp(objectCarried.transform.position,transform.position + objectCarried.GetComponent.<Properties>().pickupOffset, .5);
	}
}

function Drop(){
	//Release object constraints
	objectCarried.GetComponent.<Rigidbody>().useGravity = true;
	objectCarried.GetComponent.<Rigidbody>().isKinematic = false;
	objectCarried.GetComponent.<Collider>().enabled = true;
	//Reset objectCarried
	objectCarried = null;
	//Change the player state
	GetComponentInParent.<Player>().carrying = false;
}

function Throw(){
	
	//Apply force to the object
	if(objectCarried.GetComponent.<Properties>().throwable == true){
			//Check inputAim
			if(inputAim.normalized != Vector3(0,0,0)){
				//Release object constraints
				objectCarried.GetComponent.<Rigidbody>().useGravity = true;
				objectCarried.GetComponent.<Rigidbody>().isKinematic = false;
				objectCarried.GetComponent.<Collider>().enabled = true;
				//Apply force to the object
				objectCarried.GetComponent.<Rigidbody>().AddForce(inputAim.normalized * throwSpeed);
				//Reset objectCarried
				objectCarried = null;
				//Change the player state
				GetComponentInParent.<Player>().carrying = false;
			//Check inputMove
			}else{
				//Release object constraints
				objectCarried.GetComponent.<Rigidbody>().useGravity = true;
				objectCarried.GetComponent.<Rigidbody>().isKinematic = false;
				objectCarried.GetComponent.<Collider>().enabled = true;
				//Apply force to the object
				objectCarried.GetComponent.<Rigidbody>().AddForce(inputMove.normalized * throwSpeed);
				//Reset objectCarried
				objectCarried = null;
				//Change the player state
				GetComponentInParent.<Player>().carrying = false;
		}
	}
}

//STATE FUNCTIONS:

function Off (){
	//Set State
	currentState = TargetState.Off;
	GetComponent.<MeshRenderer>().enabled = false;
}

function Tight(){
	//Set State
	currentState = TargetState.Tight;
	GetComponent.<MeshRenderer>().enabled = true;

	//Positioning
	//Reset target position	
	transform.position = playerPosition;
	//Set new target position
	transform.position += inputMove * ringDistance; //Was inputAim instead of inputMove
	
}

function TightLocked(){
	//Set State
	currentState = TargetState.TightLocked;
	GetComponent.<MeshRenderer>().enabled = true; //was false
	
	//Positioning
	//transform.position += inputAim.normalized * ringDistance;
	
	if(inputAim.normalized != Vector3(0,0,0)){
		//Reset target position	
		transform.position = playerPosition;
		//Set new target position
		transform.position += inputAim.normalized * ringDistance;
	}else if(inputMove.normalized != Vector3(0,0,0)){
		//Reset target position	
		transform.position = playerPosition;
		//Set new target Position
		transform.position += inputMove.normalized * ringDistance;
	}
	
}

/*
function Loose(){
	//Set State
	currentState = TargetState.Loose;
}

function Range(){
	//Set State
	currentState = TargetState.Range;
}
*/
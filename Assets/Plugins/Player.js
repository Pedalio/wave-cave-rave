﻿//#pragma strict

//VARIABLES:
var walkSpeed : float = 5.0;
var sprintSpeed : float = 8.0;
var groundOffset : float = 2.15;

var facingTop : int = 1;
var facingBottom : int = 1;
var animationValueTop : int = 1;
var animationValueBottom : int = 1;

var carrying : boolean = false;

var moveSpeed : float;
var inputMove : Vector3;
var inputAim : Vector3;
private var inputSprint : float;
private var inputThrow : float;

var rotationTop : float = 0;
var rotationBottom : float = 0;

//STATES:
enum State{Idle, Walk, Run, Carry};

	var stateTop : State;
	var stateBottom : State;

//START:
function Start () {
	
	//var stateTop : State;
	//var stateBottom : State;
	
	stateTop = State.Idle;
	stateBottom = State.Idle;
	GetComponent.<MeshRenderer>().enabled = false;
	
//MAIN LOOP:	
}

function Update () {
	
	//Get Inputs
	inputMove = Vector3(Input.GetAxis("Player1_Move_H"), 0 , -Input.GetAxisRaw("Player1_Move_V"));
	inputAim = Vector3(Input.GetAxis("Player1_Move_H"), 0 , -Input.GetAxisRaw("Player1_Move_V")); //Copied inputMove to keep right stick from being used
	//inputAim = Vector3(Input.GetAxis("Player1_Aim_H"), 0 , -Input.GetAxisRaw("Player1_Aim_V"));
	//inputSprint = Input.GetKey(KeyCode.Joystick1Button4);
	//inputThrow = Input.GetKey(KeyCode.Joystick1Button5);
	inputCarry = Input.GetKeyDown(KeyCode.Joystick1Button16);//(KeyCode.Joystick1Button0); 0 or 12?
	inputResetCam = Input.GetKeyDown(KeyCode.Joystick1Button6);
	
	//Get Rotations
	GetRotations();
	
	//check cam reset
	if(inputResetCam){
		UnityEngine.VR.InputTracking.Recenter(); 
	}
	
	//Check idle/walking/running
	if(inputMove.magnitude > 0){
		if(inputSprint > 0){
			Sprint();
			//increase throwSpeed
			GetComponentInChildren.<TargetRing>().throwSpeed = 1600;
		}else{
			Walk();
			//decrease throwSpeed
			GetComponentInChildren.<TargetRing>().throwSpeed = 800;
		}
	}else{
		Idle();
	}

	if (carrying){
		Carry();
	}

	/*
	//Check carrying
	if(carrying == false){
	
		if(inputCarry){
			//check if there's something to pickup
			GetComponentInChildren.<TargetRing>().Carry();	
		}
	}else{
		//act if it's still true
		GetComponentInChildren.<TargetRing>().Carry();
		//check drop
		if(inputCarry){
			GetComponentInChildren.<TargetRing>().Drop();
		}
		//Check throw
		if(inputThrow > 0){
			GetComponentInChildren.<TargetRing>().Throw();
		}
	}
	//groundCheck
	groundCheck();
	*/
	
	//Movement
	GetComponent.<Rigidbody>().position += (inputMove * moveSpeed * Time.deltaTime);
	
	//Draw Ray
	Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down)*5, Color.red);
		
}

//FUNCTIONS:
function groundCheck(){
	
	//Reset gravity to be on
	//GetComponent.<Rigidbody>().useGravity = true;
	
	//stairCheck
	var stairCheck : RaycastHit;
	if (Physics.Raycast (transform.position, Vector3.down, stairCheck, 5)){
		
		//Check if its a staircase
		if(stairCheck.transform.GetComponent.<Properties>() != null && stairCheck.transform.gameObject.GetComponent.<Properties>().staircase == true){
				onStairs = true;
				//Turn off gravity & move to ground
				GetComponent.<Rigidbody>().useGravity = false;
				GetComponent.<Rigidbody>().position.y -= (stairCheck.distance - groundOffset);
		}else{
			//Reset gravity to be on
			GetComponent.<Rigidbody>().useGravity = true;
			onStairs = false;
		}
	}
}



function GetRotations(){
	
	//TOP ANGLE
	
	//Get Euler angles from analoog stick input
	if(inputAim.magnitude > .02){
		if(inputAim.x >= 0){
			rotationTop = Vector3.Angle(Vector3(0,0,1),inputAim);
		}else{
			rotationTop = Vector3.Angle(Vector3(0,0,-1),inputAim) + 180;
		}
	
		//Determine top facing based on angle
		if((rotationTop >= 337.5 || rotationTop <= 22.5)){
			facingTop = 1; // up
		}else if((rotationTop >= 22.5 && rotationTop <= 67.5)){
			facingTop = 2; // up/right
		}else if((rotationTop >= 67.5 && rotationTop <= 112.5)){
			facingTop = 3; // right
		}else if((rotationTop >= 112.5 && rotationTop <= 157.5)){
			facingTop = 4; // down/right
		}else if((rotationTop >= 157.5 && rotationTop <= 202.5)){
			facingTop = 5; // down
		}else if((rotationTop >= 202.5 && rotationTop <= 247.5)){
			facingTop = 6; // down/left
		}else if((rotationTop >= 247.5 && rotationTop <= 292.5)){
			facingTop = 7; // left
		}else if((rotationTop >= 292.5 && rotationTop <= 337.5)){
			facingTop = 8; // up/left
		}
	}
	
	//BOTTOM ANGLE
	
	//Get Euler angles from analog stick input
	if(inputMove.magnitude > .02){
		if(inputMove.x >= 0){
			rotationBottom = Vector3.Angle(Vector3(0,0,1),inputMove);
		}else{
			rotationBottom = Vector3.Angle(Vector3(0,0,-1),inputMove) + 180;
		}
		
		//Determine top facing based on angle
		if((rotationBottom >= 337.5 || rotationBottom <= 22.5)){
			facingBottom = 1; // up
		}else if((rotationBottom >= 22.5 && rotationBottom <= 67.5)){
			facingBottom = 2; // up/right
		}else if((rotationBottom >= 67.5 && rotationBottom <= 112.5)){
			facingBottom = 3; // right
		}else if((rotationBottom >= 112.5 && rotationBottom <= 157.5)){
			facingBottom = 4; // down/right
		}else if((rotationBottom >= 157.5 && rotationBottom <= 202.5)){
			facingBottom = 5; // down
		}else if((rotationBottom >= 202.5 && rotationBottom <= 247.5)){
			facingBottom = 6; // down/left
		}else if((rotationBottom >= 247.5 && rotationBottom <= 292.5)){
			facingBottom = 7; // left
		}else if((rotationBottom >= 292.5 && rotationBottom <= 337.5)){
			facingBottom = 8; // up/left
		}
	}	
		
	//Automatic Facing
	if(inputAim.magnitude < .02 && inputMove.magnitude > .02){
		facingTop = facingBottom;
	}
	
	//if(inputMove.magnitude < .02 && inputAim.magnitude > .02){
	if(inputAim.magnitude > .02){
		facingBottom = facingTop;
	}
	
	//DEBUG
	//Debug.Log("Top = " + stateTop);
	//Debug.Log("Bottom = " + stateBottom);		
}


//STATE FUNCTIONS:

function Idle(){
	stateTop = State.Idle;
	stateBottom = State.Idle;
	animationValueTop = 1;
	animationValueBottom = 1;
}

function Walk(){
	stateTop = State.Walk;
	stateBottom = State.Walk;	
	moveSpeed = walkSpeed;	
	animationValueTop = 2;
	animationValueBottom = 2;	
}

function Sprint(){
	stateTop = State.Run;
	stateBottom = State.Run;	
	moveSpeed = sprintSpeed;
	animationValueTop = 3;
	animationValueBottom = 3;	
}

function Carry(){	
	carrying = true;
	stateTop = State.Carry;
	animationValueTop = 4;	
}




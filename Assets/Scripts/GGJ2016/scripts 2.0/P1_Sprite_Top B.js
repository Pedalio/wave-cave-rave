﻿//#pragma strict

//VARIABLES:
var frameCount : int = 8; //Number of collumns in sprite sheet 
var angleCount : int = 128; //Number of rows in sprite sheet                         
var framesPerSecond : float = 10.0;
var backwards : boolean = false;
var angleFacing : int = 5;
var animationState : int = 1;

private var yOffset : int = 1; //Determines which row (angle) to animate

//MAIN LOOP:
function Update () {
	
	//stairOffset();
	
	//SPRITESHEET INDEX / ANIMATION
	
	//Update the offset to match the facing angle
	animationState = GetComponentInParent.<Player>().animationValueTop;
	//print("animationstate: " + animationState);
	
	angleFacing = GetComponentInParent.<Player>().facingTop;
	
	yOffset = angleFacing + (animationState * 8) - 8;
	
	//Calculate index

	var index : int = (Time.time * framesPerSecond);

	// repeat when exhausting all frames
	index = index % (frameCount * angleCount);
 
	// Size of every tile
	var size : Vector2 = Vector2(1.0 / frameCount, 1.0 / angleCount);
 
	// split into horizontal and vertical index
	var uIndex : float = index % frameCount;
 
	// build offset
	// v coordinate is the bottom of the image in opengl so we need to invert.	
	
	//Check Backwards
	if(backwards == true){
		framesPerSecond = -10;
		backwardsValue = (size.x * frameCount)-(size.x);
	}else{
		framesPerSecond = 10;
		backwardsValue = 0;
	}
	//var offset = Vector2 ((uIndex * size.x)+((size.x * frameCount)-(size.x)), 1.0 - size.y * yOffset); //ANIMATES BACKWARDS
	//var offset = Vector2 ((uIndex * size.x), 1.0 - size.y * yOffset);
	var offset = Vector2 ((uIndex * size.x)+backwardsValue, 1.0 - size.y * yOffset);

	
	GetComponent.<Renderer>().material.SetTextureOffset ("_MainTex", offset);
	GetComponent.<Renderer>().material.SetTextureScale ("_MainTex", size);


}
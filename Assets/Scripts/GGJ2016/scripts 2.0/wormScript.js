﻿//#pragma strict
var rotationSpeed : float = 4;
var target : Transform;
var distance : float;
var speed : float = 75;
var targetDistance : float = 1.5;

private var lookRotation : Quaternion;
private var direction : Vector3;

function Start () {

}

function FixedUpdate () {
	Seek();
}

function Seek(){
	distance = Vector3.Distance(transform.position, target.position);
	
	if(distance > targetDistance){
		transform.position = Vector3.MoveTowards(transform.position, target.position, distance * speed * .01);
	}//else{

	direction = (target.position - transform.position).normalized;
 
    //create the rotation we need to be in to look at the target
    lookRotation = Quaternion.LookRotation(direction);

   	//rotate us over time according to speed until we are in the required rotation
    transform.rotation = Quaternion.Slerp(transform.localRotation, lookRotation, Time.deltaTime * rotationSpeed);
}

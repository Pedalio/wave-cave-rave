﻿//#pragma strict

//PUBLIC VARIABLES

var target : Transform;
var zOffset : float = 18;
var height : float = 15;
var positionDamping : float = 3.0;

//PRIVATE VARIABLES

// Place the script in the Camera-Control group in the component menu
@script AddComponentMenu("Camera-Control/Camera Follow")

function LateUpdate () {
	// Early out if we don't have a target
	if (!target)
		return;
	
	var wantedPosition = Vector3(target.position.x + 5, target.position.y + height, target.position.z - zOffset);
	var currentPosition = Vector3(transform.position.x, transform.position.y, transform.position.z);
	
	currentPosition = Vector3.Lerp(currentPosition, wantedPosition, positionDamping * Time.deltaTime);
	
	transform.position = Vector3(currentPosition.x, currentPosition.y ,currentPosition.z);
		
	//LOOK AT THE TARGET
	//transform.LookAt (target);
	//transform.rotation.y = 0;
	//transform.rotation.z = 0;
}
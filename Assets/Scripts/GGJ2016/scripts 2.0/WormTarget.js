﻿//#pragma strict

var borderBottom : float = 10;
var borderTop : float = 35;
var borderRight : float = 25;
var borderLeft : float = -30;
var borderFront : float = 20;
var borderBack : float = -30;

var collisionTimer : float = 100;

function Start () {

}

function Update () {
	//wiggle
	transform.position += Vector3(Random.Range(-.1,.1),Random.Range(-.02,.02),Random.Range(-.1,.1));
	//vertical cap
	if(transform.position.y > borderTop){
		transform.position.y = borderTop - 10;
		
	}
	if(transform.position.y < borderBottom){
		transform.position.y = borderBottom + 10;
	}
	//horizontal cap
	if(transform.position.x > borderRight){
		transform.position.x = borderRight - 10;
	}
	if(transform.position.x < borderLeft){
		transform.position.x = borderLeft + 10;
	}
	//depth cap
	if(transform.position.z > borderFront){
		transform.position.z = borderFront - 10;
	}
	if(transform.position.z < borderBack){
		transform.position.z = borderBack + 5;
	}
	/*
	Debug.DrawLine(Vector3(borderLeft,0,0), Vector3(borderRight,0,0));
	Debug.DrawLine(Vector3(0,borderTop,0), Vector3(0,borderBottom,0));
	Debug.DrawLine(Vector3(0,0,borderFront), Vector3(0,0,borderBack));
	*/
	collisionTimer -= .1;
	
	if(collisionTimer < 0){
		transform.position += Vector3(Random.Range(-10,10),Random.Range(-4,4),Random.Range(-10,10));
		collisionTimer = 100;
	}
		
}

function OnTriggerEnter(other : Collider){
	if(other.GetComponent.<wormScript>() != null){
		transform.position += Vector3(Random.Range(-10,10),Random.Range(-2,2),Random.Range(-10,10));
		collisionTimer = 100;
	}
}
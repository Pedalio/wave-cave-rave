﻿//#pragma strict

//Public Variables
var positionTarget : GameObject;
var rotationTarget : GameObject;
var frameCount : int = 8; //Number of collumns in sprite sheet 
var angleCount : int = 8; //Number of rows in sprite sheet 
//var segment : int = 0; //Which animation is being played                         
var framesPerSecond : float = 10.0;
var backwards : boolean = false;
var spriteOffset : Vector3;
var magnitude : float;
var speed : float;
var angleFacing : int = 5;

var textureIdle : Texture;
var textureWalk : Texture;
var textureRun : Texture;

var targetObj : Transform;
var angleOther : float;


//Private Variables
private var yOffset : int = 1; //Determines which row (angle) to animate


function Update () {
	
	//Get inputs
	angleOther = targetObj.GetComponent(P1_Sprite_Bottom).angleFacing;
	
	//top half
	inputAim = Vector3(Input.GetAxisRaw("Player1_Aim_H"), 0 , -Input.GetAxisRaw("Player1_Aim_V"));
	magnitude = inputAim.magnitude;
	//bottom half
	inputMove = Vector3(Input.GetAxisRaw("Player1_Move_H"), 0 , -Input.GetAxisRaw("Player1_Move_V"));
	speed = inputMove.magnitude;
	//sprint
	inputSprint = Input.GetAxisRaw("Player1_Trigger_L");


		//Facing animation direction
	if((rotationTarget.transform.eulerAngles.y >= 337.5 || rotationTarget.transform.eulerAngles.y <= 22.5) && (magnitude >= .01)){
		angleFacing = 1; // up
	}
	if((rotationTarget.transform.eulerAngles.y >= 22.5 && rotationTarget.transform.eulerAngles.y <= 67.5)){ //&& (magnitude >= .01)){
		angleFacing = 2; // up/right
	}
	if((rotationTarget.transform.eulerAngles.y >= 67.5 && rotationTarget.transform.eulerAngles.y <= 112.5)){// && (magnitude >= .01)){
		angleFacing = 3; // right
	}
	if((rotationTarget.transform.eulerAngles.y >= 112.5 && rotationTarget.transform.eulerAngles.y <= 157.5)){// && (magnitude >= .01)){
		angleFacing = 4; // down/right
	}
	if((rotationTarget.transform.eulerAngles.y >= 157.5 && rotationTarget.transform.eulerAngles.y <= 202.5)){// && (magnitude >= .01)){
		angleFacing = 5; // down
	}
	if((rotationTarget.transform.eulerAngles.y >= 202.5 && rotationTarget.transform.eulerAngles.y <= 247.5)){// && (magnitude >= .01)){
		angleFacing = 6; // down/left
	}
	if((rotationTarget.transform.eulerAngles.y >= 247.5 && rotationTarget.transform.eulerAngles.y <= 292.5)){// && (magnitude >= .01)){
		angleFacing = 7; // left
	}
	if((rotationTarget.transform.eulerAngles.y >= 292.5 && rotationTarget.transform.eulerAngles.y <= 337.5)){// && (magnitude >= .01)){
		angleFacing = 8; // up/left
	}
	
	//Automatic Facing
	if(magnitude < .01 && speed >= .01){
		angleFacing = angleOther;
	}

	
	//SPRITESHEET INDEX / ANIMATION
	
	//Update the offset to match the facing angle
	yOffset = angleFacing; //SHOULD EQUAL NEW "Y" VALUE (Y = angleFacing + (Z*8))

	
	
	// Calculate index
	if(backwards == true){
		framesPerSecond = -10;
	}else{
		framesPerSecond = 10;
	}
	var index : int = (Time.time * framesPerSecond);

	// repeat when exhausting all frames
	index = index % (frameCount * angleCount);
 
	// Size of every tile
	var size : Vector2 = Vector2(1.0 / frameCount, 1.0 / angleCount);
 
	// split into horizontal and vertical index
	var uIndex : float = index % frameCount;
 
	// build offset
	// v coordinate is the bottom of the image in opengl so we need to invert.
	
	
	//Check Backwards
	if(backwards == true){
		backwardsValue = (size.x * frameCount)-(size.x);
	}else{
		backwardsValue = 0;
	}
	//var offset = Vector2 ((uIndex * size.x)+((size.x * frameCount)-(size.x)), 1.0 - size.y * yOffset); //ANIMATES BACKWARDS
	//var offset = Vector2 ((uIndex * size.x), 1.0 - size.y * yOffset);
	var offset = Vector2 ((uIndex * size.x)+backwardsValue, 1.0 - size.y * yOffset);
	

	//Texture Switching
	//if(speed >= .01 && speed <= .8){
	//	GetComponent.<Renderer>().material.mainTexture = textureWalk;
	//}else if(speed > .8){
	//	GetComponent.<Renderer>().material.mainTexture = textureRun;
	
	if((inputSprint != 1) && (speed >= .01)){
		GetComponent.<Renderer>().material.mainTexture = textureWalk;
	}else if((inputSprint == 1) && (speed >= .01)){
		GetComponent.<Renderer>().material.mainTexture = textureRun;
	}else{
		GetComponent.<Renderer>().material.mainTexture = textureIdle;
	}
	
	GetComponent.<Renderer>().material.SetTextureOffset ("_MainTex", offset);
	GetComponent.<Renderer>().material.SetTextureScale ("_MainTex", size);
	
	//Stick to the rotationTarget player object
	transform.position = positionTarget.transform.position + spriteOffset;

}
#pragma strict

//PUBLIC VARIABLES
//var target : GameObject;
var speed : float;

//PRIVATE VARIABLES
private var inputMove : Vector3;

function Start () {
}


function Update () {

	//Stick to the target player object
	//transform.position = target.transform.position;

	//Get input
	inputMove = Vector3(Input.GetAxisRaw("Player2_Move_H"), 0 , -Input.GetAxisRaw("Player2_Move_V"));
	speed = inputMove.magnitude;
	
	
	//Rotation
	transform.rotation = Quaternion.LookRotation(inputMove);

}


﻿//#pragma strict

//PUBLIC VARIABLES
var xPosition : float;
var yPosition : float;
var height : float = 1.75;

var target1 : Transform;
var target2 : Transform;

//PRIVATE VARIABLES

private var rightCheck : float;
private var leftCheck : float;
private var upCheck : float;
private var downCheck : float;

private var distanceX : float;
private var distanceY : float;

private var centerTarget : Vector3;

function Update () {

	//CHECK BORDERS
	//Check right border
	rightCheck += 1000;
	if(rightCheck > target1.position.x){ //check player 1
		rightCheck = target1.position.x;
	}
	if(rightCheck > target2.position.x){ //check player 2
		rightCheck = target2.position.x;
	}
	
	//Check left border
	leftCheck -= 1000;
	if(leftCheck < target1.position.x){ //check player 1
		leftCheck = target1.position.x;
	}
	if(leftCheck < target2.position.x){ //check player 2
		leftCheck = target2.position.x;
	}
	
	//Check top border
	upCheck += 1000;
	if(upCheck > target1.position.z){ //check player 1
		upCheck = target1.position.z;
	}
	if(upCheck > target2.position.z){ //check player 2
		upCheck = target2.position.z;
	}

	//Check bottom border
	downCheck -= 1000;
	if(downCheck < target1.position.z){ //check player 1
		downCheck = target1.position.z;
	}
	if(downCheck < target2.position.z){ //check player 2
		downCheck = target2.position.z;
	}
	
	
	//CALCULATE DISTANCE
	distanceX = Mathf.Abs(leftCheck - rightCheck);
	distanceY = Mathf.Abs(downCheck - upCheck);
	
	//FIND CENTER
	xPosition = rightCheck + (distanceX/2);
	yPosition = upCheck + (distanceY/2);
	
	centerTarget = Vector3(xPosition,height,yPosition);

	transform.position = centerTarget;

}
﻿using UnityEngine;
using System.Collections;

public class LineTargetingTest : MonoBehaviour {
	//variables
	private float triggerLeft;
	private float triggerRight;
	public Color color;

	// Use this for initialization
	public void Start () {
		gameObject.GetComponent<Renderer>().material.color = color;
	}
	
	// Update is called once per frame
	public void Update () {

		//Get Inputs
		triggerLeft = Input.GetAxisRaw("Player1_Trigger_L");
		triggerRight = Input.GetAxisRaw("Player1_Trigger_R");

		//Change colors
		if(triggerRight == 1){
			gameObject.GetComponent<Renderer>().enabled = true;
			color = Color.green;
		}else{
			gameObject.GetComponent<Renderer>().enabled = false;
		}

		gameObject.GetComponent<Renderer>().material.color = color;

	}
}

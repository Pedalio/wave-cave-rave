//#pragma strict

//PUBLIC VARIABLES
//var target : GameObject;
var speed : float;

//PRIVATE VARIABLES
private var inputMove : Vector3;
private var inputAim : Vector3;

function Start () {
}


function Update () {

	//Stick to the target player object
	//transform.position = target.transform.position;

	//Get input
	inputMove = Vector3(Input.GetAxisRaw("Player1_Move_H"), 0 , -Input.GetAxisRaw("Player1_Move_V"));
	inputAim = Vector3(Input.GetAxisRaw("Player1_Aim_H"), 0 , -Input.GetAxisRaw("Player1_Aim_V"));
	speed = inputMove.magnitude;
	
	
	//Rotation
	
	//if((inputAim.magnitude > .01) && (speed < .01)){
	//	transform.rotation = Quaternion.LookRotation(inputAim);
	//}else{
		transform.rotation = Quaternion.LookRotation(inputMove);
	//}

}


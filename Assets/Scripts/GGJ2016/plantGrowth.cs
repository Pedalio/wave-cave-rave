﻿using UnityEngine;
using System.Collections;

public class plantGrowth : MonoBehaviour {

	public int growthLevel;
	public int baseGrowth;
	public float offsetY = 0.0f;
	public GameObject manager;
	public int life = 3;
	
	public Texture tex3;
	public Texture tex2;
	public Texture tex1;
	public Texture tex0;

	// Use this for initialization
	void Start () {
		//Set random start growth level
		baseGrowth = Random.Range (1,3);
		Grow ();
		//life = 3;
	}

	// Update is called once per frame
	void Update () {

		//limit life
		if (life > 4 && life < 50){
			life = 4;
		}
		
		if(life < 0){
			life = 0;
		}

		//Change Texture based on life
		
		if(life == 3){
			GetComponent<Renderer>().material.SetTexture ("_MainTex",tex3);
		}
		if(life == 2){
			GetComponent<Renderer>().material.SetTexture ("_MainTex",tex2);
		}
		if(life == 1){
			GetComponent<Renderer>().material.SetTexture ("_MainTex",tex1);
		}
		if(life == 0){
			GetComponent<Renderer>().material.SetTexture ("_MainTex",tex0);
		}
		
		//Change texture offset to match growth level
		if(growthLevel == 1){
			GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(-0.157f, offsetY);
		}
		if(growthLevel == 2){
			GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(-0.127f, offsetY);
		}
		if(growthLevel == 3){
			GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(-0.097f, offsetY);
		}
		if(growthLevel == 4){
			GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(-0.067f, offsetY);
		}
		if(growthLevel == 5){
			GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(-0.037f, offsetY);
			GetComponent<Collider>().enabled = true;
		}
		if(growthLevel == 6){
			GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(0.0f, offsetY);
			GetComponent<Collider>().enabled = true;
		}

	}

	public void Grow () {
		//GROW!!!
		//Debug.Log ("YEEP");
		//growthLevel = baseGrowth + manager.GetComponent<TimeCycle>().dayCounter;

		if (life > 1) {
			growthLevel += 1;
		}
			life -= 1;

		//Limit growth level
		if(growthLevel > 6){
			growthLevel = 6;
		}

//		//Change Texture based on life
//
//		if(life == 3){
//			GetComponent<Renderer>().material.SetTexture ("_MainTex",tex3);
//		}
//		if(life == 2){
//			GetComponent<Renderer>().material.SetTexture ("_MainTex",tex2);
//		}
//		if(life == 1){
//			GetComponent<Renderer>().material.SetTexture ("_MainTex",tex1);
//		}
//		if(life == 0){
//			GetComponent<Renderer>().material.SetTexture ("_MainTex",tex0);
//		}
//
//		//Change texture offset to match growth level
//		if(growthLevel == 1){
//			GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(-0.157f, offsetY);
//		}
//		if(growthLevel == 2){
//			GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(-0.127f, offsetY);
//		}
//		if(growthLevel == 3){
//			GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(-0.097f, offsetY);
//		}
//		if(growthLevel == 4){
//			GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(-0.067f, offsetY);
//		}
//		if(growthLevel == 5){
//			GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(-0.037f, offsetY);
//			GetComponent<Collider>().enabled = true;
//		}
//		if(growthLevel == 6){
//			GetComponent<MeshRenderer>().material.mainTextureOffset = new Vector2(0.0f, offsetY);
//			GetComponent<Collider>().enabled = true;
//		}
	}
}

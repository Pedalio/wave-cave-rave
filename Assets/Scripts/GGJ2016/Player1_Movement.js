﻿//#pragma strict

//Public Variables
var walkSpeed : float = 4;
var sprintSpeed : float = 8;
var xCap : float = 40;
var yCap : float = 20;
var distanceX : float;
var distanceY : float;
var groundOffset : float = 0.23;
var camControl : GameObject;


//Private Variables
private var moveSpeed : float;
private var inputMove : Vector3;
private var inputAim : Vector3;
private var inputSprint : float;

function Start () {

}

function Update () {
	
	//Get Inputs
	inputMove = Vector3(Input.GetAxisRaw("Player1_Move_H"), 0 , -Input.GetAxisRaw("Player1_Move_V"));
	inputAim = Vector3(Input.GetAxisRaw("Player1_Aim_H"), 0 , -Input.GetAxisRaw("Player1_Aim_V"));
	//inputSprint = Input.GetKey(KeyCode.Joystick1Button11);
	inputSprint = Input.GetAxisRaw("Player1_Trigger_L");
	
	//Sprint
	if(inputSprint == 1){
		moveSpeed = sprintSpeed;
	}else{
		moveSpeed = walkSpeed;
	}
	
	//Sprint
	//if(inputSprint) {
	//	moveSpeed = sprintSpeed;
	//}else{
	//	moveSpeed = walkSpeed;
	//}
	
	
	//Rotation
	//transform.rotation = Quaternion.LookRotation(inputAim);
	//moveRotation = Vector3.Angle(blank,inputMove);
	
	//GroundCheck
	//var down = transform.TransformDirection (Vector3.down);
	var groundCheck : RaycastHit;
	if (Physics.Raycast (transform.position, Vector3.down, groundCheck, 3)){
		var distanceToGround = groundCheck.distance;
		//Move to ground
		GetComponent.<Rigidbody>().position.y -= (distanceToGround - groundOffset);
		//print("distanceToGround = " + distanceToGround);
	}
	//Draw Ray
	Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down)*3, Color.red);
	
	//Movement
	GetComponent.<Rigidbody>().position += (inputMove * moveSpeed * Time.deltaTime);
	
	
	
		
}
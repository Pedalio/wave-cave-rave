﻿//#pragma strict

//Public Variables
var dayLength : float; //length of day in minutes
var dayCounter : int; //number of days
var currentTime : float;
var pause : boolean = false;
private var plant : GameObject;
//var targetLight : GameObject;

//Private Variables
var maxTime : float; //Maximum day time (in seconds)
var sunRotation : float;

function Start(){
	currentTime = 0;
	//sunRotation = 270;
}

function Update () {
	
	//reset
	resetButton = Input.GetKey(KeyCode.Joystick1Button9);
	
	if(resetButton){
		currentTime = -160;
	}

	maxTime = dayLength*60;
	
	if(pause == false){
		currentTime += Time.deltaTime; //increase current time	
	}

	//NEW DAY TRIGGER
	if(currentTime > maxTime){ //Loop timer
		currentTime = 0;
		dayCounter += 1;
		//PLANT GROWTH TRIGGER FUNCTION :^)
	}

	//CurrentTime * 360 / maxTime = SunRotation
	sunRotation = (currentTime * (360/maxTime));
	
	//Rotate
	transform.rotation.eulerAngles = Vector3(0,0,sunRotation);
	
	/*
	//Turn off light during night
	if(sunRotation < 270 && sunRotation > 90){
		targetLight.SetActive (false);
	}else{
		targetLight.SetActive (true);
	}
	*/
}
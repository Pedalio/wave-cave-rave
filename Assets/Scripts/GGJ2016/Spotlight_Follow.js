﻿//#pragma strict

//Public Variables
var target : GameObject;
var height : float = 30;
var xOffset : float;
var zOffset : float;


//Private Variables

function Update () {

	transform.position = target.transform.position + Vector3(xOffset,height,zOffset); //follow the target
	
}
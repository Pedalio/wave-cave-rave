﻿#pragma strict

//Public Variables
var walkSpeed : float = 4;
var sprintSpeed : float = 8;
var xCap : float = 40;
var yCap : float = 20;
var distanceX : float;
var distanceY : float;
var camControl : GameObject;


//Private Variables
private var moveSpeed : float;
private var inputMove : Vector3;
private var inputAim : Vector3;
private var inputSprint : boolean;

function Start () {

}

function Update () {
	
	inputMove = Vector3(Input.GetAxisRaw("Player2_Move_H"), 0 , -Input.GetAxisRaw("Player2_Move_V"));
	inputAim = Vector3(Input.GetAxisRaw("Player2_Aim_H"), 0 , -Input.GetAxisRaw("Player2_Aim_V"));
	inputSprint = Input.GetKey(KeyCode.Joystick2Button11);
	
	//Sprint
	if(inputSprint) {
		moveSpeed = sprintSpeed;
	}else{
		moveSpeed = walkSpeed;
	}
	
	
	//Rotation
	//transform.rotation = Quaternion.LookRotation(inputAim);
	//moveRotation = Vector3.Angle(blank,inputMove);

		
	//Movement
	GetComponent.<Rigidbody>().position += (inputMove * moveSpeed * Time.deltaTime);
	

	
}

function FixedUpdate () {
	

}
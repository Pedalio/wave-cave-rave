#pragma strict

//PUBLIC VARIABLES
var target : GameObject;
var offset : Vector3 = Vector3(0,1.75,0);

//PRIVATE VARIABLES
private var inputAim : Vector3;

function Start () {
}


function Update () {

	//Stick to the target player object
	transform.position = target.transform.position + offset;

	//Get input
	inputAim = Vector3(Input.GetAxisRaw("Player2_Aim_H"), 0 , -Input.GetAxisRaw("Player2_Aim_V"));
	
	//Rotation
	transform.rotation = Quaternion.LookRotation(inputAim);

}


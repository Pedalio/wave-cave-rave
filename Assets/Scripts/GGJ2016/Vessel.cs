﻿using UnityEngine;
using System.Collections;

public class Vessel : MonoBehaviour {

	public bool filled = true;
	public GameObject waterfill;
	//public int counter = 50;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(filled == true){
			waterfill.GetComponent<MeshRenderer>().enabled = true;
		}else{
			waterfill.GetComponent<MeshRenderer>().enabled = false;
		}

//		if(counter < 0){
//				
//			//flicker kinematic to fix bounce glitch
//			if(GetComponent<Rigidbody>().isKinematic == true){
//				GetComponent<Rigidbody>().isKinematic = false;
//				counter = 50;
//			}else if(GetComponent<Rigidbody>().isKinematic == false){
//				GetComponent<Rigidbody>().isKinematic = true;
//				counter = 50;
//			}
//
//		}
//
//		counter -= 1;

	}

	void OnCollisionEnter(Collision collision){
		if (collision.gameObject.tag == "Plant"){
			collision.gameObject.GetComponent<plantGrowth>().life += 1;
			filled = false;
		}
	}
}

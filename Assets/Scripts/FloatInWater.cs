﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatInWater : MonoBehaviour {
  
    public float floatHeight = 2f;
    public float bounceDamp = 0.05f;
    public Vector3 buoyancyCenterOffset;
    public GameObject waterPlane;
   
    float waterLevel;
    float forceFactor = 0f;
    Vector3 actionPoint;
    Vector3 uplift;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        waterLevel = waterPlane.transform.position.y;

        actionPoint = transform.position + transform.TransformDirection(buoyancyCenterOffset);
        forceFactor = 3.0f - (actionPoint.y - waterLevel) / floatHeight;

        if (forceFactor > 0.0f)
        {
            uplift = -Physics.gravity * (forceFactor - GetComponent<Rigidbody>().velocity.y * bounceDamp);
            GetComponent<Rigidbody>().AddForceAtPosition(uplift, actionPoint);
        }

        Debug.Log(GetComponent<Rigidbody>().velocity.y);
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchDayNightWaters : MonoBehaviour {

    public GameObject dayWater;
    public GameObject nightWater;

    GameObject currentWater;
    bool isDay;

	// Use this for initialization
	void Start () {
        isDay = true;
        currentWater = dayWater;
    }
	
	// Update is called once per frame
	void Update () {
		if (isDay && (currentWater == nightWater))
        {
            currentWater = dayWater;
        }

        if (!isDay && (currentWater == dayWater))
        {
            currentWater = nightWater;
        }
    }
}

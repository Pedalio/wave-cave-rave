﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationTop : MonoBehaviour {
	public int frameCount = 8; //Number of collumns in sprite sheet 
	public int angleCount = 128; //Number of rows in sprite sheet
	public Transform playerDirection;
	public float walkingThreshold = 0.05f;
	float framesPerSecond = 1.0f;

	int angleFacing = 1;
	Renderer playerTopRenderer;
	Rigidbody playerRigidbody;
	Vector3 prevPosition;

	// Use this for initialization
	void Start () {
		playerTopRenderer = GetComponent<Renderer> ();
		playerRigidbody = playerDirection.GetComponent<Rigidbody> ();
		prevPosition = playerDirection.position;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		angleFacing = GetPlayerFacingDirection ();
		int index = (int) (Time.time * framesPerSecond);
		int movementType = GetMovementType ();
		//print ("idx: " + index);
		//print ("movementType: " + movementType);

		int yOffset = angleFacing + movementType * frameCount;
		//print ("angleFacing: " + angleFacing + " movementType: " + movementType);

		// Size of every tile
		Vector2 size = new Vector2(1.0f / frameCount, 1.0f / angleCount);

		// split into horizontal and vertical index
		float uIndex = index % frameCount;


		Vector2 offset = new Vector2 ((uIndex * size.x), 1.0f - size.y * yOffset);
		playerTopRenderer.material.SetTextureOffset ("_MainTex", offset);
		playerTopRenderer.material.SetTextureScale ("_MainTex", size);
		//print("offset: " + offset.ToString("F4") + " size: " + size.ToString("F4"));
	}

	int GetMovementType(){
		
		float speed = GetPlayerSpeed();
		print ("speed: " + speed);
		if (speed > walkingThreshold) {
			return 1;
		} 
		print ("STOPPED");
		return 0;

	}

	float GetPlayerSpeed(){
		Vector3 currPosition = playerDirection.position;
		float speed = Mathf.Abs ((currPosition - prevPosition).magnitude);
		prevPosition = currPosition;
		return speed;
	}

	int GetPlayerFacingDirection(){
		//Facing animation direction
		if((playerDirection.eulerAngles.y >= 337.5 || playerDirection.eulerAngles.y <= 22.5)){ //&& (magnitude >= .01)){
			return 1; // up
		}
		if((playerDirection.eulerAngles.y >= 22.5 && playerDirection.eulerAngles.y <= 67.5)){ //&& (magnitude >= .01)){
			return 2; // up/right
		}
		if((playerDirection.eulerAngles.y >= 67.5 && playerDirection.eulerAngles.y <= 112.5)){// && (magnitude >= .01)){
			return 3; // right
		}
		if((playerDirection.eulerAngles.y >= 112.5 && playerDirection.eulerAngles.y <= 157.5)){// && (magnitude >= .01)){
			return 4; // down/right
		}
		if((playerDirection.eulerAngles.y >= 157.5 && playerDirection.eulerAngles.y <= 202.5)){// && (magnitude >= .01)){
			return 5; // down
		}
		if((playerDirection.eulerAngles.y >= 202.5 && playerDirection.eulerAngles.y <= 247.5)){// && (magnitude >= .01)){
			return 6; // down/left
		}
		if((playerDirection.eulerAngles.y >= 247.5 && playerDirection.eulerAngles.y <= 292.5)){// && (magnitude >= .01)){
			return 7; // left
		}
		if((playerDirection.eulerAngles.y >= 292.5 && playerDirection.eulerAngles.y <= 337.5)){// && (magnitude >= .01)){
			return 8; // up/left
		}
		return 1;
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnderwaterTint : MonoBehaviour {

    public GameObject waterPlane;
    public GameObject mainCamera;
    public Color normalColor;
    public Color underwaterColor;    

    bool isCameraUnderwater;
    Material material;

	// Use this for initialization
	void Start () {
        mainCamera = GameObject.Find("Main Camera");
        waterPlane = GameObject.Find("Water4Simple 1");
        material = GetComponent<MeshRenderer>().materials[0];
        isCameraUnderwater = false;
    }
	
	// Update is called once per frame
	void Update () {

        if (mainCamera.transform.position.y < waterPlane.transform.position.y) 
        {
            isCameraUnderwater = true;
            SetUnderwaterColor();
        }
		
        if (mainCamera.transform.position.y >= waterPlane.transform.position.y)
        {
            isCameraUnderwater = false;
            SetNormalColor();
        }
    }

    void SetNormalColor()
    {
        material.color = normalColor;
    }

    void SetUnderwaterColor()
    {
        material.color = underwaterColor;
    }
}

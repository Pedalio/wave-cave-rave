﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterInterface : MonoBehaviour {

    public GameObject[] letters;
    public GameObject[] shrineMessages;
    public int numOfDays;
    GameObject currentMessage;

    bool isReadingMessage;
    int numOfLettersRead;
    int numOfShrineMessagesRead;

    // Use this for initialization
    void Start () {
        currentMessage = null;
        isReadingMessage = false;
        numOfDays = 0;
        // Start these at -1 to be able to read them on day 0
        numOfLettersRead = -1;
        numOfShrineMessagesRead = -1;
    }

    // Update is called once per frame
    void Update()
    {

        if((Input.GetKeyDown(KeyCode.Joystick1Button0) ||
            Input.GetKeyDown(KeyCode.Joystick1Button1) ||
            Input.GetKeyDown(KeyCode.Joystick1Button2) ||
            Input.GetKeyDown(KeyCode.Joystick1Button3) ||
            Input.GetKeyDown(KeyCode.Joystick1Button16) ||
            Input.GetKeyDown(KeyCode.Joystick1Button17) ||
            Input.GetKeyDown(KeyCode.Joystick1Button18) ||
            Input.GetKeyDown(KeyCode.Joystick1Button19)) &&
            isReadingMessage)
        {
            currentMessage.GetComponent<MeshRenderer>().enabled = false;
			print ("currenmessage: " + currentMessage.name);

            if (currentMessage == letters[numOfDays])
            {
                numOfLettersRead++;
                GameObject.Find("DayNightManagerObject").GetComponent<DayNightManager>().hasReadLetter = true;
				print ("letters match numofdays");
            }

            if (currentMessage == shrineMessages[numOfDays])
            {
                numOfShrineMessagesRead++;
                GameObject.Find("DayNightManagerObject").GetComponent<DayNightManager>().hasReadShrineMessage = true;
            }

            isReadingMessage = false;
        }
    }

    public void ReadLetter()
    {
		print ("read letter");
        if (numOfLettersRead < numOfDays)
        {
			print ("less than num days");
            letters[numOfDays].transform.position = transform.position;
            letters[numOfDays].transform.rotation = transform.rotation;
            letters[numOfDays].transform.localScale = transform.localScale;
            currentMessage = letters[numOfDays];
            isReadingMessage = true;
        }
    }

    public void ReadShrineMessage()
    {
        if (numOfShrineMessagesRead < numOfDays)
        {
            shrineMessages[numOfDays].transform.position = transform.position;
            shrineMessages[numOfDays].transform.rotation = transform.rotation;
            shrineMessages[numOfDays].transform.localScale = transform.localScale;
            currentMessage = shrineMessages[numOfDays];
            isReadingMessage = true;
        }
    }
}

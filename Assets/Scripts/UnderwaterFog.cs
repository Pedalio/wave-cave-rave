﻿using UnityEngine;
using System.Collections;

// Credits: http://wiki.unity3d.com/index.php?title=Underwater_Script

public class UnderwaterFog : MonoBehaviour
{

    //This script enables underwater effects. Attach to main camera.

    //The scene's default fog settings
    private bool defaultFog = RenderSettings.fog;
    private Color defaultFogColor = RenderSettings.fogColor;
    private float defaultFogDensity = RenderSettings.fogDensity;
    private Material defaultSkybox = RenderSettings.skybox;
    private Material noSkybox;
    private GameObject waterPlane;

    void Start()
    {
        waterPlane = GameObject.Find("Water4Simple 1");
    }

    void Update()
    {
        if (transform.position.y < waterPlane.transform.position.y)
        {
            RenderSettings.fog = true;
            RenderSettings.fogColor = new Color(0, 0.4f, 0.7f, 0.6f);
            RenderSettings.fogDensity = 0.04f;
            RenderSettings.skybox = noSkybox;
        }
        else
        {
            RenderSettings.fog = defaultFog;
            RenderSettings.fogColor = defaultFogColor;
            RenderSettings.fogDensity = defaultFogDensity;
            RenderSettings.skybox = defaultSkybox;
        }
    }
}
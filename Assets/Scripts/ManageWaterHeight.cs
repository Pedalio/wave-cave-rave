﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageWaterHeight : MonoBehaviour {

    public float dayOneHeight;
    public float dayFiveHeight;
    public GameObject waterPlane;

    float delta;

	// Use this for initialization
	void Start () {
        delta = (dayFiveHeight - dayOneHeight) / 5;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetHeightToDay(int dayNum)
    {
        float y = dayOneHeight + dayNum * delta;
        Vector3 oldPos = waterPlane.transform.position;
        waterPlane.transform.position = new Vector3(oldPos.x, y, oldPos.z);
    }
}

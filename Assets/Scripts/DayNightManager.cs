﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightManager : MonoBehaviour {

    public bool allowSleep; //Gets set after player reads the shrine message
    public bool hasReadLetter;
    public bool hasReadShrineMessage;
    public int numOfDays;
    public GameObject[] bottles;

    GameObject bottleSpawn;
	StageManager stageManager;

	// Use this for initialization
	void Start () {
        hasReadLetter = false;
        hasReadShrineMessage = false;
        allowSleep = false;
        numOfDays = 0;
        //bottles = new GameObject[5];
        bottleSpawn = GameObject.Find("BottleSpawn");
        // Spawn the first bottle
        bottles[numOfDays].transform.position = bottleSpawn.transform.position;

		stageManager = GameObject.Find ("StageManager").GetComponent<StageManager>();
		if (stageManager == null) {
			throw new UnityException ("Cannot find StageManager.");
		}
    }
	
	// Update is called once per frame
	void Update () {
		if (!allowSleep)
        {
            allowSleep = hasReadLetter && hasReadShrineMessage;
        }
        if (Input.GetKeyDown("space") && allowSleep)
        {
            EndCurrentDay();
        }
    }

    public void StartNewDay()
    {
        numOfDays++;
        GameObject.Find("LetterUI").gameObject.GetComponent<LetterInterface>().numOfDays = numOfDays;
        GameObject.Find("WaterLevelManagerObject").GetComponent<ManageWaterHeight>().SetHeightToDay(numOfDays);
        bottles[numOfDays].transform.position = bottleSpawn.transform.position;
        hasReadLetter = false;
        hasReadShrineMessage = false;
        allowSleep = false;
    }

    public void EndCurrentDay()
    {
        // Cleanup if any
		if (allowSleep) {
			stageManager.DoFadeInFadeOut ();
		}
          
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReduceSpeedUnderwater : MonoBehaviour {

    public GameObject waterPlane;
    Player playerController;

	// Use this for initialization
	void Start () {
        playerController = GetComponent<Player>();
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.y < waterPlane.transform.position.y)
            playerController.walkSpeed = 2.5f;
        else
            playerController.walkSpeed = 5;
	}
}

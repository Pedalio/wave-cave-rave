﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sleep : MonoBehaviour {

    bool isOverlapping;

	// Use this for initialization
	void Start () {
        isOverlapping = false;
    }
	
	// Update is called once per frame
	void Update () {

        if ((Input.GetKeyDown(KeyCode.Joystick1Button0) ||
            Input.GetKeyDown(KeyCode.Joystick1Button1) ||
            Input.GetKeyDown(KeyCode.Joystick1Button2) ||
            Input.GetKeyDown(KeyCode.Joystick1Button3) ||
            Input.GetKeyDown(KeyCode.Joystick1Button16) ||
            Input.GetKeyDown(KeyCode.Joystick1Button17) ||
            Input.GetKeyDown(KeyCode.Joystick1Button18) ||
            Input.GetKeyDown(KeyCode.Joystick1Button19)) &&
            isOverlapping)
        {
            GameObject.Find("DayNightManagerObject").GetComponent<DayNightManager>().EndCurrentDay();
        }   
    }

    void OnTriggerEnter(Collider other)
    {
		//print (other.gameObject.name + " in bed");
        if (other.gameObject.tag == "Player")
        {
			//print ("player sleep!");
            isOverlapping = true;
            Debug.Log(isOverlapping);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isOverlapping = false;
            Debug.Log(isOverlapping);
        }
    }
}

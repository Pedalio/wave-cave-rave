﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottlePickUp: MonoBehaviour {
	public Transform handPointer;
	public bool isWalking;
	Player player;

    GameObject letterInterface;
	GameObject nearbyBottle;
	GameObject nearbyBottlePlacer;

	GameObject carryingBottle;


	// Use this for initialization
	void Start () {
		player = GetComponent<Player> ();
        letterInterface = GameObject.Find("LetterUI");
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if (Input.GetKeyDown(KeyCode.Joystick1Button0) ||
            Input.GetKeyDown(KeyCode.Joystick1Button1) ||
            Input.GetKeyDown(KeyCode.Joystick1Button2) ||
            Input.GetKeyDown(KeyCode.Joystick1Button3) ||
            Input.GetKeyDown(KeyCode.Joystick1Button16) ||
            Input.GetKeyDown(KeyCode.Joystick1Button17) ||
            Input.GetKeyDown(KeyCode.Joystick1Button18) ||
            Input.GetKeyDown(KeyCode.Joystick1Button19))
        {	// Pick up bottle
			if (nearbyBottle != null) {
				if (carryingBottle == null) {	// If not already carrying bottle
					nearbyBottle.transform.SetParent (handPointer);
					nearbyBottle.transform.localPosition = new Vector3 (0f, 0f, 0f);
					Rigidbody bottleRigidbody = nearbyBottle.GetComponent<Rigidbody> ();
					bottleRigidbody.isKinematic = true;

					carryingBottle = nearbyBottle;
					player.carrying = true;
					print ("Carried bottle!");

                    // Also start reading the letter 
                    letterInterface.gameObject.GetComponent<LetterInterface>().ReadLetter();
                    

				} else if (nearbyBottlePlacer != null) {	// If already carrying bottle, and there is a bottlePlcaer nearby
					// Place bottle
					Transform bottlePlacerPointer = nearbyBottlePlacer.transform.Find("BottlePlacerPointer");
					print ("Bottle placer name: " + nearbyBottlePlacer.name);
					if (bottlePlacerPointer != null) {
						print ("placer pointer not null");
						carryingBottle.transform.SetParent (bottlePlacerPointer);
						carryingBottle.transform.localPosition = new Vector3 (0f, 0f, 0f);
						Rigidbody bottleRigidbody = nearbyBottle.GetComponent<Rigidbody> ();
						bottleRigidbody.isKinematic = true;

                        //Trigger Shrine Message after you place a bottle
                        letterInterface.gameObject.GetComponent<LetterInterface>().ReadShrineMessage();

                        carryingBottle = null;
						player.carrying = false;
						//player.Idle ();
					} else {
						print ("pointer null");
					}
				}

			} 
		}
		
	}

	void OnTriggerEnter(Collider collider){
		if (collider.gameObject.tag == "Bottle") {
			nearbyBottle = collider.gameObject;
			print ("Bottle!");
		} else if (collider.gameObject.tag == "BottlePlacer") {
			nearbyBottlePlacer = collider.gameObject;
			print ("bottleHolder!");
		}
	}

	void OnTriggerExit(Collider collider){
		if (collider.gameObject.tag == "Bottle") {
			if (nearbyBottle == collider.gameObject) {
				nearbyBottle = null;
				print ("bottle exit");
			}
		} else if (collider.gameObject.tag == "BottlePlacer") {
			nearbyBottlePlacer = null;
			print ("bottleHolderExit!");
		}
	}
}

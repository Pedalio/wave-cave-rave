﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour {
	// AutoFade params
	public float fadeOutTime = 2f;
	public float stayOutTime = 2f;
	public float fadeInTime = 2f;
	public DayNightManager dayNightManager;

	private static StageManager m_Instance = null;
	private Material m_Material = null;

	AutoFade autoFade;
	Fade fade;

	private void Awake()
	{
		DontDestroyOnLoad(this);
		m_Instance = this;
		m_Material = Resources.Load<Material>("Plane_No_zTest");
		#if UNITY_EDITOR
		if (m_Material == null)
		{
			var resDir = new System.IO.DirectoryInfo(System.IO.Path.Combine(Application.dataPath, "Resources"));
			if (!resDir.Exists)
				resDir.Create();
			Shader s = Shader.Find("Plane/No zTest");
			if (s == null)
			{
				string shaderText = "Shader \"Plane/No zTest\" { SubShader { Pass { Blend SrcAlpha OneMinusSrcAlpha ZWrite Off Cull Off Fog { Mode Off } BindChannels { Bind \"Color\",color } } } }";
				string path = System.IO.Path.Combine(resDir.FullName, "Plane_No_zTest.shader");
				Debug.Log("Shader missing, create asset: " + path);
				System.IO.File.WriteAllText(path, shaderText);
				UnityEditor.AssetDatabase.Refresh(UnityEditor.ImportAssetOptions.ForceSynchronousImport);
				UnityEditor.AssetDatabase.LoadAssetAtPath<Shader>("Resources/Plane_No_zTest.shader");
				s = Shader.Find("Plane/No zTest");
			}
			var mat = new Material(s);
			mat.name = "Plane_No_zTest";
			UnityEditor.AssetDatabase.CreateAsset(mat, "Assets/Resources/Plane_No_zTest.mat");
			m_Material = mat;

		}
		#endif
	}

	// Use this for initialization
	void Start () {
		autoFade = new AutoFade ();
		fade = new Fade ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.D)) {
			//AutoFade.LoadLevel ("Min's Main Level", 3f, 3f, Color.black);
			StartCoroutine(FadeOutInAndSetStage(3f, 3f, 3f, Color.black));
		}
	}

	public void DoFadeInFadeOut(){
		StartCoroutine (FadeOutInAndSetStage (fadeOutTime, stayOutTime, fadeInTime, Color.black));
	}

	IEnumerator FadeOutInAndSetStage(float aFadeOutTime, float aMiddleTime, float aFadeInTime, Color aColor){
		float t = 0.0f;
		while (t<1.0f)
		{
			yield return new WaitForEndOfFrame();
			t = Mathf.Clamp01(t + Time.deltaTime / aFadeOutTime);
			DrawQuad(aColor,t);
		}
		// End day and set new day
		dayNightManager.StartNewDay();

		t = 0.0f;
		while (t < 1.0f) {
			yield return new WaitForEndOfFrame();
			t = Mathf.Clamp01(t + Time.deltaTime / aMiddleTime);
			DrawQuad(aColor,1f);
		}
		while (t>0.0f)
		{
			yield return new WaitForEndOfFrame();
			t = Mathf.Clamp01(t - Time.deltaTime / aFadeInTime);
			DrawQuad(aColor,t);
		}
	}

	private void DrawQuad(Color aColor,float aAlpha)
	{
		aColor.a = aAlpha;
		m_Material.SetPass(0);
		GL.PushMatrix();
		GL.LoadOrtho();
		GL.Begin(GL.QUADS);
		GL.Color(aColor);   // moved here, needs to be inside begin/end
		GL.Vertex3(0, 0, -1);
		GL.Vertex3(0, 1, -1);
		GL.Vertex3(1, 1, -1);
		GL.Vertex3(1, 0, -1);
		GL.End();
		GL.PopMatrix();
	}

}

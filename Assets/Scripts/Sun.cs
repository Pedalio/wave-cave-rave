﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sun : MonoBehaviour {
	public float duskRotation;
	public float dawnRotation;
	public bool timeAdvancing;
	public float dayTimeLength;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.A)) {
			StartCoroutine (DuskToDawnAnimation (dayTimeLength));
		}
		if (Input.GetKeyDown (KeyCode.S)) {
			timeAdvancing = !timeAdvancing;
		}

	}

	IEnumerator DuskToDawnAnimation(float animationTime){
		float currRotationX;
		float time = 0f;
		Vector3 currSunEulerAngle = transform.eulerAngles;

		while (time < animationTime) {
			if (timeAdvancing) {
				currRotationX = Mathf.Lerp (duskRotation, dawnRotation, time / animationTime);
				currSunEulerAngle.x = currRotationX;
				transform.eulerAngles = currSunEulerAngle;
				print (currRotationX);
				time += Time.deltaTime;
			}
			yield return new  WaitForFixedUpdate();
		}
	}
}

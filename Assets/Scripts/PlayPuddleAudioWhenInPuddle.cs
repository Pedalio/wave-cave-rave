﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayPuddleAudioWhenInPuddle : MonoBehaviour {

	public DayNightManager dayNightManager;
	public bool inWater = false;
	public bool isWalking = false;
	public bool prevIsWalking = false;
	public bool puddleAudioPlaying = false;

	AudioSource puddleAudioSource;
	Player player;

	// Use this for initialization
	void Start () {
		puddleAudioSource = GetComponent<AudioSource> ();
		player = GetComponent<Player> ();
		puddleAudioSource.Pause ();
		print ("stopped puddle");
	}
	
	// Update is called once per frame
	void Update () {
		//print ("animation: " + player.animationValueBottom);
		isWalking = IsPlayerWalking ();
		if (StartedWalking () && inWater) {
			print ("started walking and in water");
			puddleAudioSource.Play ();
			puddleAudioPlaying = true;
		} else if (StoppedWalking ()) {
			print ("stopped walking so stopped playing");
			puddleAudioSource.Pause ();
			puddleAudioPlaying = false;
		} else if (puddleAudioPlaying && !inWater) {
			print ("audio playing and not in water");
			puddleAudioSource.Pause ();
			puddleAudioPlaying = false;
		} else if (!puddleAudioPlaying && inWater) {
			puddleAudioSource.Play ();
			puddleAudioPlaying = true;
		}

		prevIsWalking = isWalking;

	}

	bool StoppedWalking(){
		return (isWalking == false) && (prevIsWalking == true);
	}

	bool StartedWalking(){
		return (isWalking == true) && (prevIsWalking == false);
	}

	bool IsPlayerWalking(){
		return player.animationValueBottom == 2;
	}

	// Checks whether there are puddles today
	bool CheckPuddleToday(){
		// Check if on the first day
		return dayNightManager.numOfDays <= 0;
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == "Water") {	// If in water
			print("Inwater");
			inWater = true;

		}
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject.tag == "Water") {
			print ("outof WAter");
			inWater = false;
		}
	}
}

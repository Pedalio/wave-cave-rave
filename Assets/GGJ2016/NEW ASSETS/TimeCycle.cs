﻿using UnityEngine;
using System.Collections;

public class TimeCycle : MonoBehaviour {
	
	public float dayLength;//length of day in minutes
	public int dayCounter; //number of days
	public float currentTime;
	public bool pause = false;
	public float maxTime; //Maximum day time (in seconds)

	private bool resetButton;
	private float sunRotation;
	private GameObject[] plants;
	

	// Use this for initialization
	void Start () {
		currentTime = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		//reset
		resetButton = Input.GetKey(KeyCode.Joystick1Button9);
		
		if(resetButton){
			currentTime = -160f;
		}
		
		maxTime = dayLength*60;
		
		if(pause == false){
			currentTime += Time.deltaTime; //increase current time	
		}
		
		//NEW DAY TRIGGER
		if(currentTime > maxTime){ //Loop timer
			currentTime = 0f;
			dayCounter += 1;
			//PLANT GROWTH TRIGGER FUNCTION :^)
			plants = GameObject.FindGameObjectsWithTag("Plant");
			foreach(GameObject plant in plants){
				plant.GetComponent<plantGrowth>().Grow();
			}
			
		}

		//CurrentTime * 360 / maxTime = SunRotation
		sunRotation = (currentTime * (360/maxTime));
		
		//Rotate
		//transform.rotation.eulerAngles = new Vector3(0,0,sunRotation);
		GetComponent<Transform>().eulerAngles = new Vector3(0,0,sunRotation);


	}
}
